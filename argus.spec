%define _hardened_build 1
%global clientversion 3.0.8.2
Name:                argus
Version:             3.0.8.2
Release:             3
Summary:             Network transaction audit tool
License:             GPLv2+
Url:                 http://qosient.com/argus
Source0:             http://qosient.com/argus/src/%{name}-%{version}.tar.gz
Source1:             http://qosient.com/argus/src/%{name}-clients-%{clientversion}.tar.gz
Source2:             argus.service
Source3:             argus.logrotate
Patch0:              argus-tirpc.patch
Patch1:              common.patch
Requires(post): systemd-units
Requires(preun): systemd-units
Requires(postun): systemd-units
Requires:            logrotate
BuildRequires:       gcc autoconf automake cyrus-sasl-devel perl-generators flex bison ncurses-devel
BuildRequires:       libpcap-devel zlib-devel libtirpc-devel readline-devel systemd-units make

%package clients
Summary:             Client tools for argus network audit
%description
Argus (Audit Record Generation and Utilization System) is an IP network
transaction audit tool. The data generated by argus can be used for a
wide range of tasks such as network operations, security and performance
management.
%description clients
Clients to the argus probe which process and display information.

%package devel
Summary:             Header files for argus network audit
%description devel
Header files for argus.

%prep
%setup -a0 -q
%setup -a1 -q
%patch0 -p0
%patch1 -p0
sed -i 's/vm_stat/vmstat/g' events/argus-vmstat.sh

%build
autoreconf -if
export CPPFLAGS="$CPPFLAGS -I/usr/include/tirpc -fcommon"
export CXXFLAGS="$CXXFLAGS -fcommon"
%configure --with-sasl=yes --prefix=%{_prefix}
find . -type f -name 'Makefile' | xargs sed -i s/fcf-protection/fcf-protection\ -fcommon/g
pushd %{name}-clients-%{clientversion}
%configure --with-sasl=yes --prefix=%{_prefix}
%{__make} %{?_smp_mflags}
popd

%install
%{__make} DESTDIR=%{buildroot} install
pushd %{name}-clients-%{clientversion}
%{__make} DESTDIR=%{buildroot} install
find support -type f -exec chmod a-x '{}' \;
popd
%{__rm} -rf %{buildroot}/%{_libdir}
%{__install} -d -m 0755 %{buildroot}/%{_localstatedir}/lib/argus/archive
%{__install} -D -m 0644 support/Config/argus.conf %{buildroot}/%{_sysconfdir}/argus.conf
%{__install} -D -m 0644 %{SOURCE2} %{buildroot}/%{_unitdir}/argus.service
%{__sed} -i 's|#ARGUS_SET_PID|ARGUS_SET_PID|' %{buildroot}/%{_sysconfdir}/argus.conf
%{__sed} -i 's|#ARGUS_PID_PATH|ARGUS_PID_PATH|' %{buildroot}/%{_sysconfdir}/argus.conf
%{__sed} -i 's|var/log/argus|var/lib/argus|' %{buildroot}/%{_sysconfdir}/argus.conf
find support -type f -exec chmod a-x '{}' \;
rm -rf %{buildroot}/usr/share/doc/argus-clients-3.0/
mv %{buildroot}/usr/argus %{buildroot}%{_datadir}/argus
mkdir -p %{buildroot}%{_sysconfdir}/logrotate.d
%{__install} -p -D -m 644 %{SOURCE3} %{buildroot}%{_sysconfdir}/logrotate.d/argus

%post
%systemd_post argus.service

%preun
%systemd_preun argus.service

%postun
%systemd_postun_with_restart argus.service

%files
%doc support bin/argusbug
%doc CREDITS INSTALL README VERSION
%license COPYING
%config(noreplace) %{_sysconfdir}/argus.conf
%config(noreplace) %{_sysconfdir}/logrotate.d/argus
%{_unitdir}/argus.service
%{_bindir}/argus*
%{_sbindir}/argus
%{_mandir}/man5/argus*
%{_mandir}/man8/argus*
%dir %{_localstatedir}/lib/argus
%dir %{_localstatedir}/lib/argus/archive

%files clients
%doc %{name}-clients-%{clientversion}/ChangeLog %{name}-clients-%{clientversion}/COPYING
%doc %{name}-clients-%{clientversion}/CREDITS %{name}-clients-%{clientversion}/INSTALL
%doc %{name}-clients-%{clientversion}/README %{name}-clients-%{clientversion}/VERSION
%doc %{name}-clients-%{clientversion}/support
%{_bindir}/ra*
%{_sbindir}/ra*
%{_mandir}/man1/ra*
%{_mandir}/man5/ra*
%{_mandir}/man8/ra*
%{_datadir}/argus/

%files devel
%{_includedir}/argus/

%changelog
* Mon Jun 26 2023 Ge Wang <wang__ge@126.com> - 3.0.8.2-3
- replace command from vm_stat to vmstat

* Wed Mar 01 2023 Ge Wang <wangge20@h-partners.com> - 3.0.8.2-2
- change service type to forking

* Mon Sep 6 2021 wulei <wulei80@huawei.com> - 3.0.8.2-1
- package init
